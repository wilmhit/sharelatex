
# Overleaf community compose

To launch:
```
docker compose up -d
```

## To create admin user
```
docker compose exec sharelatex /bin/bash -c "grunt user:create-admin --email=joe@example.com"
```
More at:

[Managing users](https://github.com/overleaf/overleaf/wiki/Creating-and-managing-users)

